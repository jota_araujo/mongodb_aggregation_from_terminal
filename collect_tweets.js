const chalk = require('chalk')
const TwitService = require('./twit_service')
const credentials = require('./credentials')
const monk = require('monk')

const url = 'localhost:27017/pokemongo'
const db = monk(url)
const collection = db.get('tweets')
const tweets = [];

function saveTweets(arr) {
  collection.insert(arr)
  .then((docs) => {
    console.log(docs.length, ' items saved to the database.')
  })
  .catch((err) => {
    console.log(err)
  })
  .then(() => db.close())
}

function processTweet(tweet) {
  const doc = {
    created_at : tweet.created_at,
    tweet_id: tweet.id,
    text: tweet.text,
    hashtags: tweet.entities.hashtags.map(function (n) {
      return n.text;
    }),
    user : {
      id: tweet.user.id,
      name: tweet.user.name,
      screen_name: tweet.user.screen_name,
      description: tweet.user.description,
      followers_count: tweet.user.followers_count,
      friends_count: tweet.user.friends_count,
      time_zone: tweet.user.time_zone,
      profile_image_url: tweet.user.profile_image_url,
    },
  }
  tweets.push(doc)
  if (tweets.length > 50) {
    saveTweets(tweets.splice(0, 50))
    //console.log(tweets.length, ' ', chalk.bold(doc.user.screen_name), ': ', doc.text)
  }
  
}


db.then(() => {
  console.log('Connected to mongo')
  const twitService = new TwitService(credentials.consumer_key, credentials.consumer_secret,
    credentials.access_token, credentials.access_token_secret);
  twitService.startStream({ track: ['pokemon'],
                              language: ['en'] },
                              processTweet, console.log);
})

