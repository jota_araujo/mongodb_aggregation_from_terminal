require('js-contains')
const chalk = require('chalk')
const monk = require('monk')
const yargs = require('yargs')
var prettyjson = require('prettyjson');
const PipelineBuilder = require('./mongo_util').PipelineBuilder

const url = 'localhost:27017/pokemongo'
const db = monk(url)

function runAggregationFromTerminalInput(collection) {

  const builder = new PipelineBuilder()

  // filter out non user generated props from argv array
  const excludedProps = ['_', '$0', 'collection']
  const cmds = Object.keys(yargs.argv).filter(n => excludedProps.contains(n) === false)

  // pass along user commands to the pipeline builder
  cmds.forEach((cmd) => {
    yargs.array(cmd)
    builder.addCommand(cmd, yargs.argv[cmd])
  })

  const pipeline = builder.build()

  // run the aggregation query with monk
    console.log('--------------- Generated Query:')
  console.log(JSON.stringify(pipeline, null, 2))
  console.log('------------------Result:')
  return collection.aggregate(pipeline).then((res) => {
    res.forEach(n => console.log(prettyjson.render(n)))
    db.close()
  })
  .catch((err) => {
    console.log(err)
    db.close()
  })
}

db.then(() => {
  console.log('Connected to mongo.')
  runAggregationFromTerminalInput(db.get('tweets'))

})
.catch((err) => {
  console.log(err)
  db.close()
})
