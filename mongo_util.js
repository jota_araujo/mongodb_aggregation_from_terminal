require('js-contains')

function PipelineBuilder() {
  this.steps = []
  this.accumulators = ['addToSet', 'sum']
  this.string_types = ['limit', 'unwind']
  this.object_types = ['sort']
  this.aliases = new Map()
}

PipelineBuilder.prototype.createGroupStepOnce = function () {
  if (!this.groupStep) {
    this.groupStep = { $group : { _id: {} } }
    this.steps.push(this.groupStep)
  }
}

PipelineBuilder.prototype.stripAlias = function (value) {
  if (typeof value === 'string' && value.includes(':')) {
    const arr = value.split(':')

    this.aliases.set(arr[1], arr[0])
    return arr[1]
  }
  return value
}


PipelineBuilder.prototype.withDolar = function (str) {
  if (str.indexOf('$') !== 0) {
    return '$' + str
  }
  return str
}

PipelineBuilder.prototype.maybeAlias = function (str) {
  if (this.aliases.get(str)) {

    return this.aliases.get(str)
  }
  return str
}

PipelineBuilder.prototype.build = function () {
  return this.steps
}

PipelineBuilder.prototype.mapValuesToProperties = function (obj, propNames, parentObj) {

  propNames.forEach((name) => {
    obj[this.maybeAlias(name)] = isNaN(name) ? this.withDolar(name) : name;
  })

  if (parentObj) {
    this.steps.push(parentObj)
  }
}

PipelineBuilder.prototype.parseAccumulator = function (accum, values) {
  this.createGroupStepOnce()
  this.groupStep.$group[this.maybeAlias(values[0])] = {}
  let propVal
  if (values.length > 1) {
    propVal = isNaN(values[1]) ? this.withDolar(values[1]) :  values[1]
  } else {
    propVal = 1
  }
  this.groupStep.$group[values[0]][this.withDolar(accum)] = propVal
}

PipelineBuilder.prototype.parseToProjection = function (values) {

  this.projectStage = { $project: {} }

  values.forEach(name => {
    const alias = this.maybeAlias(name)
    if (alias) {
      this.projectStage.$project[alias] = this.withDolar(name)
    } else {
      this.projectStage.$project[name] = 1
    }
  })

  this.steps.push(this.projectStage)

}

PipelineBuilder.prototype.parseToObject = function (command, values, addDolar) {

  const propVal = isNaN(values[1]) ? this.withDolar(values[1]) : values[1]

  const parentObj = {}
  const obj = parentObj[this.withDolar(command)] = {}

  obj[addDolar ? this.withDolar(values[0]) : values[0]] = propVal

  this.steps.push(parentObj)
}

PipelineBuilder.prototype.parseToString = function (command, values) {

  const parentObj = {}
  parentObj[this.withDolar(command)] = isNaN(values[0]) ? this.withDolar(values[0]) : values[0];
  this.steps.push(parentObj)

}


PipelineBuilder.prototype.addCommand = function (command, values) {

  const asc = values.indexOf('asc')
  if (asc !== -1) {
    values[asc] = -1
  }

  values = values.map(n => this.stripAlias(n))

  if (command === 'group') {
    this.createGroupStepOnce()
    this.mapValuesToProperties(this.groupStep.$group._id, values, null)
    return
  }

  if (command === 'noId') {
    this.projectStage.$project._id = 0
    return
  }


  if (this.accumulators.contains(command)) {
    this.parseAccumulator(command, values)
    return
  }

  if (command === 'project') {
    this.parseToProjection(values)
  }

  if (this.string_types.contains(command)) {
    this.parseToString(command, values)
    return
  }

  if (this.object_types.contains(command)) {
    this.parseToObject(command, values)
    return
  }
}

module.exports.PipelineBuilder = PipelineBuilder