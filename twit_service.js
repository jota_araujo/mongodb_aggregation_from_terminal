const Twit = require('twit');

class TwitService {

  constructor(consumer_key, consumer_secret, access_token, access_token_secret) {

    this.twitInstance = new Twit({
      consumer_key,
      consumer_secret,
      access_token,
      access_token_secret,
      timeout_ms: 30 * 1000,
    });

    this.prevTime = new Date().getTime();
    this.skippedCount = 0;

  }

  startStream(options, handler, log) {
    if (handler == null) throw new Error('Missing handler function parameter');

    if (this.stream != null) {
      this.stream.stop();
    }

    const stream = this.twitInstance.stream('statuses/filter', options);

    stream.on('connect', function (__request) {
      log('TwitService::STREAM CONNECTED');
    });

    stream.on('disconnect', function (__request) {
      log('TwitService::STREAM DISCONNECTED');
    });

    stream.on('warning', function (warning) {
      log('TwitService::STREAM WARNING');
      log(warning);
    });

    stream.on('error', function (err) {
      log('TwitService::STREAM ERROR:');
      log(err);
    });

    stream.on('tweet', function (tweet) {
      // if we get too many, skip to prevent server overload
      // if (new Date().getTime() > this.prevTime + 500) {
      handler(tweet, log);
      // } else {
      // this.skippedCount++;
      // }
    });
  }
}

module.exports = TwitService;