# Description:

An experiment on generating MongoDB aggregation queries from simple terminal arguments using Node.js

# Screenshots:

Top 3 Hashtags from collected stream of pokemon related tweets:  

![Top 3 hashtags](https://bytebucket.org/jota_araujo/mongodb_aggregation_from_terminal/raw/0c83ccd7548aec60ff431cfa95f4bfa23dfd219f/screenshots/screenshot1.png)

Top 3 users by number of followers:  
  
![Top 3 users by followers](https://bytebucket.org/jota_araujo/mongodb_aggregation_from_terminal/raw/0c83ccd7548aec60ff431cfa95f4bfa23dfd219f/screenshots/screenshot2.png)